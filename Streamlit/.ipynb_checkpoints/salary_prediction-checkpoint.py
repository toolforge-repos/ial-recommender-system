import streamlit as st
import pandas as pd

DATA="survey_results_public.csv"


def load_data(nrows):
    data = pd.read_csv(DATA, nrows=nrows)
    return data
nrows=10000
data=load_data(nrows)


if st.checkbox('Show raw data'):
    st.subheader('Raw data')
    st.write(data)

if st.checkbox('Show histograms'):
    st.subheader('Number of pickups by hour')
    hist_values=data.YearsCode
    st.bar_chart(hist_values)